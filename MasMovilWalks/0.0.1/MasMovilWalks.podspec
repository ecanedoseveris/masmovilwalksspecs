Pod::Spec.new do |spec|
  spec.name             = "MasMovilWalks"
  spec.version          = "0.0.1"
  spec.summary          = "A short description of MasMovilWalks"
  spec.homepage         = "https://gitlab.com"
  spec.license          = { :type => "MIT", :file => "LICENSE" }
  spec.author           = { "Everis" => "ecanedos@everis.com" }
  spec.platform         = :ios, "13.0"
  spec.swift_version    = "5.0"
  spec.static_framework = true
  spec.source           = { :http => "https://gitlab.com/ecanedoseveris/masmovilwalksspecs/raw/main/MasMovilWalks/0.0.1/MasMovilWalks-0.0.1.zip" }
  spec.source_files     = "MasMovilWalks", "Soter/**/*.{h,m,swift}"
  spec.resource_bundles = { 'MasMovilWalks' => ['MasMovilWalks/App/Resources/**/*.lproj', 'MasMovilWalks/**/*.xcassets', 'MasMovilWalks/**/*.{png,jpeg,jpg,html,json,mp3,xib}'] }
  spec.exclude_files    = ['MasMovilWalks/docs']
  spec.ios.vendored_frameworks = ['MasMovilWalks.framework']
  # spec.dependency 'VideoID' , '~> 7.1.9'
end
